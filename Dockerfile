# builder image
FROM golang:latest as builder
WORKDIR /build
COPY . /build
RUN CGO_ENABLED=0 GOOS=linux go build -o gateway-service .

# generate clean, final image for end users
FROM alpine:3.15 as hoster
COPY --from=builder /build/gateway-service ./gateway-service

# executable
ENTRYPOINT [ "./gateway-service" ]
