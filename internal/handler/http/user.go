package http

import (
	"database/sql"
	"encoding/json"
	"errors"
	"identity-service/internal/domain/dto"
	"identity-service/internal/service/auth"
	"identity-service/pkg/server/response"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type Identity struct {
	authService *auth.Service
}

func NewIdentityHandler(authService *auth.Service) *Identity {
	return &Identity{
		authService: authService,
	}
}

func (h *Identity) Routes() chi.Router {
	r := chi.NewRouter()

	r.Route("/", func(r chi.Router) {
		r.Post("/sign-up", h.SignUp)
		r.Post("/sign-in", h.SignIn)
	})

	return r
}

func (h *Identity) SignUp(w http.ResponseWriter, r *http.Request) {
	req := dto.Request{}
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		response.BadRequest(w, r, err, nil)
		return
	}

	res, err := h.authService.SignUp(r.Context(), req)
	switch err {
	case auth.ErrUserExist:
		response.BadRequest(w, r, err, nil)
	case nil:
		response.OK(w, r, res)
	default:
		response.InternalServerError(w, r, err)
	}
}

func (h *Identity) SignIn(w http.ResponseWriter, r *http.Request) {
	req := dto.Request{}
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		response.BadRequest(w, r, err, nil)
		return
	}

	res, err := h.authService.SignIn(r.Context(), req)
	switch err {
	case sql.ErrNoRows:
		response.BadRequest(w, r, errors.New("неправильно введены данные"), nil)
	case nil:
		response.OK(w, r, res)
	default:
		response.InternalServerError(w, r, err)
	}

}
