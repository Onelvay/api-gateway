package handler

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"identity-service/internal/config"
	"identity-service/internal/handler/http"
	"identity-service/internal/service/auth"
	"identity-service/pkg/server/router"
)

type Dependencies struct {
	Configs config.Configs

	AuthService *auth.Service
}

// Configuration is an alias for a function that will take in a pointer to a Handler and modify it
type Configuration func(h *Handler) error

// Handler is an implementation of the Handler
type Handler struct {
	dependencies Dependencies

	HTTP *chi.Mux
}

// New takes a variable amount of Configuration functions and returns a new Handler
// Each Configuration will be called in the order they are passed in
func New(d Dependencies, configs ...Configuration) (h *Handler, err error) {
	// Insert the handler
	h = &Handler{
		dependencies: d,
	}

	// Apply all Configurations passed in
	for _, cfg := range configs {
		// Pass the service into the configuration function
		if err = cfg(h); err != nil {
			return
		}
	}

	return
}

func WithHTTPHandler() Configuration {
	return func(h *Handler) (err error) {
		h.HTTP = router.New()

		h.HTTP.Use(middleware.Timeout(h.dependencies.Configs.APP.Timeout))

		identityHandler := http.NewIdentityHandler(h.dependencies.AuthService)
		h.HTTP.Route("/", func(r chi.Router) {
			r.Mount("/auth", identityHandler.Routes())
		})

		return
	}
}
