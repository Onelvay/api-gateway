package app

import (
	"context"
	"flag"
	"fmt"
	"go.uber.org/zap"
	"identity-service/internal/config"
	"identity-service/internal/handler"
	"identity-service/internal/provider/account"
	"identity-service/internal/service/auth"
	"identity-service/pkg/log"
	"identity-service/pkg/server"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func Run() {
	logger := log.LoggerFromContext(context.Background())

	accountClient := account.New(account.Credentials{
		Endpoint: "http://localhost:8000",
	})

	authService, err := auth.New(
		auth.WithAccountClient(accountClient),
	)
	if err != nil {
		logger.Error("ERR_INIT_SERVICE", zap.Error(err))
		return
	}

	handlers, err := handler.New(handler.Dependencies{
		Configs:     config.Configs{},
		AuthService: authService,
	},
		handler.WithHTTPHandler(),
	)
	servers, err := server.New(
		server.WithHTTPServer(handlers.HTTP, "2004"))
	// Run our server in a goroutine so that it doesn't block.
	if err != nil {
		logger.Error("ERR_INIT_SERVERS", zap.Error(err))
		return
	}

	if err = servers.Run(logger); err != nil {
		logger.Error("ERR_RUN_SERVERS", zap.Error(err))
		return
	}

	logger.Info("Server started on http://localhost:" + "2004" + "/swagger/index.html")

	// Graceful Shutdown
	var wait time.Duration
	flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.Parse()

	quit := make(chan os.Signal, 1) // Create channel to signify a signal being sent

	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.

	signal.Notify(quit, os.Interrupt, syscall.SIGTERM) // When an interrupt or termination signal is sent, notify the channel
	<-quit                                             // This blocks the main thread until an interrupt is received
	fmt.Println("Gracefully shutting down...")

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()

	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	if err = servers.Stop(ctx); err != nil {
		panic(err) // failure/timeout shutting down the server gracefully
	}

	fmt.Println("Running cleanup tasks...")
	// Your cleanup tasks go here

	fmt.Println("Server was successful shutdown.")
}
