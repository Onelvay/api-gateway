package auth

import (
	"identity-service/internal/provider/account"
)

type Configuration func(s *Service) error

type Service struct {
	accountClient *account.Client
}

func New(configs ...Configuration) (s *Service, err error) {
	s = &Service{}

	for _, cfg := range configs {
		if err = cfg(s); err != nil {
			return
		}
	}

	return
}

func WithAccountClient(accountClient *account.Client) Configuration {
	return func(s *Service) error {
		s.accountClient = accountClient
		return nil
	}
}
