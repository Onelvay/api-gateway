package auth

import (
	"context"
	"errors"
	"go.uber.org/zap"

	"identity-service/internal/domain/dto"
	"identity-service/internal/provider/account"
	"identity-service/pkg/log"
)

var (
	ErrUserExist = errors.New("user with such login is already exist")
)

func (s *Service) SignIn(ctx context.Context, req dto.Request) (res dto.Response, err error) {
	logger := log.LoggerFromContext(ctx).Named("SignIn")

	err = s.accountClient.GetUserByLogin(ctx, account.Identity{
		Login:    req.Login,
		Password: req.Password,
	})
	if err != nil {
		logger.Error("failed to get user by login", zap.Error(err))
		return
	}

	token, err := GenerateToken(req.Login)
	if err != nil {
		return
	}

	res = dto.Response{
		AccessToken: token,
	}g
	return
}

func (s *Service) SignUp(ctx context.Context, req dto.Request) (res dto.Response, err error) {
	//logger := log.LoggerFromContext(ctx).Named("SignUp")

	_, err = s.accountClient.CreateUser(ctx, account.Identity{
		Login:    req.Login,
		Password: req.Password,
	})
	if err != nil {
		err = ErrUserExist
		//logger.Error("failed to create user", zap.Error(err))
		return
	}

	res = dto.Response{
		AccessToken: "accessToken",
	}
	return
}
