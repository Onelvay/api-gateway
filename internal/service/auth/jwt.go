package auth

import (
	"context"
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	"time"
)

var code = []byte("")

func GenerateToken(login string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"login": login,
		"exp":   time.Now().Add(12 * time.Hour).Unix(),
		"iat":   time.Now().Unix(),
	})
	tokenString, _ := token.SignedString(code)
	return tokenString, nil
}

func parseToken(tokenString string) (jwt.MapClaims, error) {
	token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("wrong token")
		}
		return code, nil
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	}
	return nil, fmt.Errorf("token if not validated")
}

func GetLoginFromToken(ctx context.Context, tokenString string) (login string, err error) {
	claims, err := parseToken(tokenString)
	if err != nil {
		return
	}

	login = claims["login"].(string)
	fmt.Println(login)
	return
}
