package account

import (
	"io"
	"net/http"
	"time"
)

type Credentials struct {
	Endpoint string
	Username string
	Password string
}

type Client struct {
	httpClient *http.Client
	credential Credentials
}

func New(credential Credentials) *Client {
	httpClient := http.DefaultClient
	httpClient.Timeout = 30 * time.Second

	client := &Client{
		httpClient: httpClient,
		credential: credential,
	}

	return client
}

func (client *Client) request(method, path, contentType string, reqBytes io.Reader) (resBytes []byte, err error) {
	return
}
