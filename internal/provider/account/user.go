package account

import (
	"context"
)

type Identity struct {
	ID       string `bson:"id"`
	Login    string `bson:"username"`
	Password string `bson:"password"`
}

func (c *Client) GetUserByLogin(ctx context.Context, req Identity) (err error) {
	return
}

func (c *Client) CreateUser(ctx context.Context, req Identity) (res Identity, err error) {

	return
}
